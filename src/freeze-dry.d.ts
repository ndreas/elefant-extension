declare module 'freeze-dry' {
    export default function freezeDry(doc: Document): Promise<string>;
}
