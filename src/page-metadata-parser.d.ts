declare module 'page-metadata-parser' {
    export function getMetadata(doc: Document, url: string): any;
}
