interface Config {
    backend?: string
    token?: string
}

browser.pageAction.onClicked.addListener(async tab => {
    if (!tab.id) return;

    let config: Config = await browser.storage.sync.get(['backend', 'token']);

    if (!config.backend) {
        browser.runtime.openOptionsPage();
        return;
    }

    try {
        await browser.tabs.executeScript(tab.id, {
            file: '/content.js'
        });

        let [content, screenshot] = await Promise.all([
            browser.tabs.sendMessage(tab.id, { serializePage: true }),
            browser.tabs.captureTab(tab.id, { format: 'png' }),
        ]);

        let url = new URL('/documents/', config.backend);
        let headers: any = { 'Content-Type': 'application/json' };

        if (config.token) {
            headers['Authorization'] = 'Bearer ' + config.token;
        }

        let res = await fetch(url.href, {
            headers,
            method: 'POST',
            body: JSON.stringify({
                screenshot,
                ...content
            })
        });

        if (res.ok) {
            browser.notifications.create({
                type: 'basic',
                title: 'Elefant',
                message: 'Page successfully saved',
            });
        } else {
            browser.notifications.create({
                type: 'basic',
                title: 'Failed to save document in Elefant',
                message: res.statusText,
            });
        }
    } catch (err) {
        browser.notifications.create({
            type: 'basic',
            title: 'Elefant error',
            message: err.toString(),
        });
    }
});
