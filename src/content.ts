import { getMetadata } from 'page-metadata-parser';
import freezeDry from 'freeze-dry';

browser.runtime.onMessage.addListener(async (msg, sender, res) => {
    if (!msg.serializePage) return;

    let metadata = getMetadata(document, window.location.href);
    let html = await freezeDry(document);

    return { html, metadata };
});
