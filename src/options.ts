function backendInput(): HTMLInputElement {
    return document.querySelector("input#backend")! as HTMLInputElement;
}
function tokenInput(): HTMLInputElement {
    return document.querySelector("input#token")! as HTMLInputElement;
}

document.querySelector('form')!.addEventListener('submit', ev => {
    ev.preventDefault();
    browser.storage.sync.set({
        backend: backendInput().value,
        token: tokenInput().value,
    })
});

document.addEventListener('DOMContentLoaded', async () => {
    let res = await browser.storage.sync.get(['backend', 'token']);
    backendInput().value = res.backend || '';
    tokenInput().value = res.token || '';
});
