const Copy = require('copy-webpack-plugin');

module.exports = {
    entry: {
        background: './src/background.ts',
        content: './src/content.ts',
        options: './src/options.ts',
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/dist'
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [
            { test: /\.ts$/, loader: 'awesome-typescript-loader' }
        ]
    },

    plugins: [
        new Copy([
            { from: './src/manifest.json', to: '.' },
            { from: './src/icons', to: './icons' },
            { from: './src/options.html', to: '.' },
        ])
    ]
}
